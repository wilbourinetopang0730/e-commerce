// Import Packages
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

// Import Components
import AppNavBar from './components/AppNavBar';

// Import Pages
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error'
import Products from './pages/Products'

// Import Admin Pages
import Dashboard from './pages/Admin/Dashboard.js';
import ProductUpdate from './pages/Admin/Product-Update.js';
import ProductCreate from './pages/Admin/Product-Create.js';
import UserOrderHistory from './pages/Admin/User-Order.js';

// Import User Pages
import MyProfile from './pages/User/MyProfile.js';
import ViewProducts from './pages/ProductsView'

import './App.css';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({ id: null, isAdmin: null });

  const unsetUser = () => {
        localStorage.clear();
  }

  useEffect(() => {
        if(localStorage.getItem('token')) {
            fetch("https://e-commerce-9otx.onrender.com/users/view/", {
                headers: {
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                }
            })
            .then(response => response.json())
            .then(data => {
                setUser({
                     id: data._id,
                     isAdmin: data.isAdmin
                })
            })
        }
    }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
    <Router>      
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/products" element={<Products />} />
            <Route path="/*" element={<Error />} /> 

            // Admin Routes
            <Route path="/admin/dashboard" element={<Dashboard />} />
            <Route path="/admin/product/update" element={<ProductUpdate />} />
            <Route path="/admin/product/create" element={<ProductCreate />} />
            <Route path="/admin/user/orderhistory" element={<UserOrderHistory />} />
            
            // User Routes
            <Route path="/myprofile" element={<MyProfile />} />
            <Route path="/products/view" element={<ViewProducts />} />

          </Routes>
        </Container>
    </Router>

    </UserProvider>
  );
}

export default App;
