import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return(
		<Row className="mt-3 mb-3">

			<Col xs={ 12 } md={ 4 }>
				<Card className="cardHighlight p-3 card text-center">
					<Card.Body>
						<Card.Title>
							<h2>Hot Item #1</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tempor felis. Suspendisse vitae lobortis libero. Sed condimentum nisl ac orci dapibus, a consectetur sem sagittis. Mauris sodales tempus dignissim. Maecenas semper.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={ 12 } md={ 4 }>
				<Card className="cardHighlight p-3 card text-center">
					<Card.Body>
						<Card.Title>
							<h2>Hot Item #2</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lobortis vestibulum auctor. Curabitur cursus elit sem, sed efficitur nisi volutpat in. Ut commodo tellus a metus vulputate, id tempor magna faucibus. In.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={ 12 } md={ 4 }>
				<Card className="cardHighlight p-3 card text-center">
					<Card.Body>
						<Card.Title>
							<h2>Hot Item #3</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis nisi eu interdum volutpat. Mauris luctus velit in neque tempus interdum. Aliquam eget elit leo. Aenean ultrices tincidunt nisl ut imperdiet. Sed.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>
	)
}
