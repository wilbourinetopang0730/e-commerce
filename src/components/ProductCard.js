// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function ViewProductUser({ product }) {

	const { user, setUser } = useContext(UserContext)
	const { productId, setProductId } = useState('');
	const { _id, name, description, price, isActive, createdOn } = product;
	
	function viewProduct () {
		localStorage.setItem('productId', product._id);

	}
	return (
		<>
        	<Col xs={ 12 } md={ 4 } align="center" className="p-3 mb-3">
        		<Card className="cardProduct p-3 card text-center">
        			<Card.Body>
        				<Card.Title><h3>{ product.name }</h3></Card.Title>
        				<Card.Text>{ description }</Card.Text>

        				<Link to='/products/view'><Button variant="primary" onClick={ viewProduct }>View</Button></Link>
        			</Card.Body>
        		</Card>
        		<br/ >
        	</Col>
        </>
	)
}