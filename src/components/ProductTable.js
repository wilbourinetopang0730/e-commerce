import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function ViewProductAdmin({ product }) {
	const { user, setUser } = useContext(UserContext)
	const { productId, setProductId } = useState('');
	const { _id, name, description, price, isActive, createdOn } = product;

	let realDate = createdOn.slice(0,10);
	let firstFour = _id.slice(0, 4);
	let lastFour = _id.substr(_id.length - 4);

	function updateProduct () {
		localStorage.setItem('productId', product._id);
	}

	function updateProduct () {
		localStorage.setItem('productId', product._id);
	}

	function activateProduct () {
		// Pop up message
		Swal.fire({
		  title: 'Are you sure?',
		  text: "Setting this product to active?",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Activate Product API
		  	fetch(`${process.env.REACT_APP_API_URL}/products/activate/${product._id}`, {
				method: "PATCH",
	    		headers: {
	    			Authorization: `Bearer ${ localStorage.getItem('token') }`
	    		}
			}).then(response => response.json()).then(data => data)

		    Swal.fire({
		      title: 'Activated!',
		      text: 'Product set to true!',
		      icon: 'success',
		      timer: 2000
		    })
		  }
		})
	}

	function disableProduct () {
		// Pop up message
		Swal.fire({
		  title: 'Are you sure?',
		  text: "Setting this product to not active?",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Deactivate Product API
		  	fetch(`${process.env.REACT_APP_API_URL}/products/archive/${product._id}`, {
				method: "PATCH",
	    		headers: {
	    			Authorization: `Bearer ${ localStorage.getItem('token') }`
	    		}
			}).then(response => response.json()).then(data => data)

		    Swal.fire({
		      title: 'Deactivated!',
		      text: 'Product set to false!',
		      icon: 'success',
		      timer: 2000

		    })
		  }
		})
	}

	return (
		<>
    	<tr>
    		<td>{firstFour}.......{lastFour}</td>
    		<td>{product.name}</td>
    		<td className="productDescription">{product.description}</td>
    		<td>{product.price}</td>
    		{
    		(isActive) ?
    		<td>True</td> : <td>False</td>
			}
    		<td>{realDate}</td>
    		<td>
				<Link to='/admin/product/update'><Button className="adminBtn" variant="success" onClick={ updateProduct }>Update</Button></Link>
				{
					(product.isActive) ?
					<Button className="adminBtn" variant="danger" onClick={ disableProduct }>Disable</Button>
					:
					<Button className="adminBtn" variant="primary" onClick={ activateProduct }>Enable</Button>
				}
    		</td>
    	</tr>
</>
	)
}