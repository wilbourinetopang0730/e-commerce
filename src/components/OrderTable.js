import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function ViewProductAdmin({ order }) {
	const { user, setUser } = useContext(UserContext)

	const [ orders, setOrders ] = useState([])
	const { _id, userId, totalAmount, products, purchasedOn } = order;

	let realDate = purchasedOn.slice(0,10);
	let firstFour = _id.slice(0, 4);
	let lastFour = _id.substr(_id.length - 4);
	return (
		<>
		<tr>
    		<td>{firstFour}.......{lastFour}</td>
    		<td>{order.totalAmount}</td>
    		<td>{realDate}</td>
    	</tr>
		</>
	)




}