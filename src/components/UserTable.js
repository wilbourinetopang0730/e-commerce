import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function ViewUserAdmin({ userTable }) {
	const { user, setUser } = useContext(UserContext)
	const { userId, setUserId } = useState('');
	const { _id, firstName, lastName, email, password, isAdmin } = userTable;

	let firstFour = password.slice(0, 4);
	let lastFour = password.substr(password.length - 4);

	function viewOrderHistory () {
		localStorage.setItem('userId', userTable._id);
	}
	function changeAdminStatus(e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

     	if (userTable.isAdmin) {
     		fetch(`${process.env.REACT_APP_API_URL}/users/admin`, { 
	    		method: "PATCH",
		    		headers: {
		    			"Authorization": `Bearer ${ localStorage.getItem('token') }`,
						"Content-Type": "application/json"
		    		},
		    		body: JSON.stringify({
		    			email: email,
		    			isAdmin: false
		    		})
			}).then(response => response.json()).then(data => data)
    	} else {
    		fetch(`${process.env.REACT_APP_API_URL}/users/admin`, { 
    		method: "PATCH",
	    		headers: {
	    			"Authorization": `Bearer ${ localStorage.getItem('token') }`,
					"Content-Type": "application/json"
	    		},
	    		body: JSON.stringify({
	    			email: email,
	    			isAdmin: true
	    		})
			}).then(response => response.json()).then(data => data)
    	}
    }



	return (
		<>
    		<tr>
    			<td>{ _id }</td>
    			<td>{ firstName }</td>
    			<td>{ lastName }</td>
    			<td>{ email }</td>
    			<td>{ firstFour }......{ lastFour }</td>
    			{
	    		(isAdmin) ?
	    			<td className="text-success">True</td> : <td className="text-danger">False</td>
				}
    			<td>

    			{
    			(isAdmin) ?
					<Button className="adminBtn" variant="primary" disabled>Orders</Button>
				:
					<Link to='/admin/user/orderhistory'><Button className="adminBtn" variant="primary" onClick={viewOrderHistory}>Orders</Button></Link>
				}
				{
				(isAdmin) ?
					<Button className="adminBtn" variant="danger" onClick={changeAdminStatus}>Admin</Button>
				:
					<Button className="adminBtn" variant="success" onClick={changeAdminStatus}>Admin</Button>
				}
    			</td>

    		</tr>
		</>
	)
}