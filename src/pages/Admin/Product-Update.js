import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function UpdateProduct() {
	const { user, setUser } = useContext(UserContext)
	const [ products, setProducts ] = useState([])
	const productId = localStorage.getItem("productId");

	// State Hooks for updating the product
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/view`, {
			method: "POST",
    		headers: {
	        		"Content-Type": "application/json"
	        },
    		body: JSON.stringify({
    			id: productId
    		})
		}).then(response => response.json()).then(data => setProducts(data));
	}, [])

	// Function Start
	function updateProduct (e) {
		// Prevents page redirection via form submission
    	e.preventDefault();
    	
    	// Pop up message
		Swal.fire({
		  title: 'Are you sure?',
		  text: "Product will be updated if you press 'OK'",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Update Product API
		  	fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
	    		method: "PUT",
	    		headers: {
	    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
	    			"Content-Type": "application/json"
	    		},
	    		body: JSON.stringify({
	    			name: productName,
	    			description: description,
	    			price: price
	    		})
	    	}).then(response => response.json()).then(data => data);
		    Swal.fire({
		      title: 'Updated!',
		      text: 'Product is now updated!',
		      icon: 'success',
		      timer: 2000
		    })
		  }
		})

	}

	return (
		(user.isAdmin) ?
		<>
			<h3>{products.name}</h3>
			<hr />

			<Form onSubmit={(e) => updateProduct(e)}>
				{/* Product Name */}
				<Form.Group controlId="productName">
					<Form.Label>Product Name</Form.Label>
					<Form.Control
						type="text"
						placeholder={ products.name }
						value={ productName }
						onChange={e => setProductName(e.target.value)}
						required
					 />
				</Form.Group>

				{/* Description */}
				<Form.Group controlId="productDescription">
					<Form.Label>Product Description</Form.Label>
					<Form.Control
						type="text"
						placeholder={ products.description }
						value={ description }
						onChange={e => setDescription(e.target.value)}
						required
					 />
				</Form.Group>

				{/* Price */}
				<Form.Group controlId="productPrice">
					<Form.Label>Product Price</Form.Label>
					<Form.Control
						type="number"
						placeholder={ products.price }
						value={ price }
						onChange={e => setPrice(e.target.value)}
						required
					 />
				</Form.Group>

				{/* Buttons */}
				<Button variant="success" type="submit" className="mt-2" to="/admin/dashboard">Update</Button>
			</Form>
		</>
		:
			<Navigate to="/" />
	)
}