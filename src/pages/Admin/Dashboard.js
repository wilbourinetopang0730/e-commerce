// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

// Imported Components
import ProductTable from '../../components/ProductTable';
import UserTable from '../../components/UserTable';

// JS File Default Function
export default function AdminDashboard() {
	const { user, setUser } = useContext(UserContext)

	// variable used for mapping
	const [ products, setProducts ] = useState([])
	const [ userTable, setUserTable ] = useState([])
	
	// useEffect to fetch the products and map it to the dashboard
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/viewAll`, { 
				headers: {
	        		authorization: `Bearer ${ localStorage.getItem('token') }`
	        	}
	    }).then(response => response.json()).then(data => {
	    	setProducts(data.map(product => {
	    		return (
	    			<ProductTable key={ product._id } product = { product } />
	    		)
	    	}))
	    }).catch(error => error);
	})

	// useEffect to fetch the products and map it to the dashboard
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/viewAll`, { 
				headers: {
	        		authorization: `Bearer ${ localStorage.getItem('token') }`
	        	}
	    }).then(response => response.json()).then(data => {
	    	setUserTable(data.map(userTable => {
	    		return (
	    			<UserTable key={ userTable._id } userTable = { userTable } />
	    		)
	    	}))
	    }).catch(error => error);
	})
	
	// Start Function DELETE ALL
	function deleteAll(e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

    	// Pop up message
		Swal.fire({
		  title: 'Are you sure?',
		  text: "All products will be deleted if you press 'OK'",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Update Product API
		  	fetch(`https://e-commerce-9otx.onrender.com/products/delete`, {
	    		method: "DELETE",
	    		headers: {
	    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
	    			"Content-Type": "application/json"
	    		}
	    	}).then(response => response.json()).then(data => data);
		    Swal.fire({
		      title: 'Deleted!',
		      text: 'All products are now deleted!',
		      icon: 'success',
		      timer: 2000
		    })
		  }
		})
	}

	return (
		(user.isAdmin) ?
		<>	
			<h2>Admin User Dashboard</h2>
			<Link to='/admin/product/create'><Button variant="secondary" className="me-2">Create New Product</Button></Link>
			<Button variant="danger" onClick={ deleteAll }>DELETE ALL</Button>
			<hr />

			<h3>Table of Product Registered</h3>
			<div id="table-wrapper">
  			<div id="table-scroll">

			  <table className="table">
				<thead>
			    	<tr>
					    <th width="14%">Product ID</th>
					    <th width="14%">Product Name</th>
					    <th width="28%">Product Description</th>
					    <th width="14%">Product Price</th>
					    <th width="7%">isActive</th>
					    <th width="14%">createdOn</th>
					    <th width="7%">Actions</th>
					</tr>
			 	</thead>

			 	<tbody>
					{ products }
				</tbody>
			  </table>

		  </div>
		  </div>

			<h3>Table of User Registered</h3>
			<div id="table-wrapper">
  			<div id="table-scroll">
			
			  <table className="table">
				<thead>
			    	<tr>
			    		<th width="14%">User ID</th>
					    <th width="14%">First Name</th>
					    <th width="14%">Last Name</th>
					    <th width="14%">Email Address</th>
					    <th width="7%">Password</th>
					    <th width="14%">isAdmin</th>
					    <th width="7%">Actions</th>
					</tr>
			 	</thead>

			 	<tbody>
					{ userTable }
				</tbody>
			  </table>
		  
		  </div>
		  </div>
		</>
		:
		<Navigate to="/" />
	)
}