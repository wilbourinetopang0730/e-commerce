import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

import OrderTable from '../../components/OrderTable';

export default function UserOrders() {
	const { user, setUser } = useContext(UserContext)
	const [ orders, setOrders ] = useState([])
	const user_id = localStorage.getItem("userId");

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/viewAdmin`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
    			userId: user_id
    		})


		}).then(response => response.json()).then(data => {
	    	setOrders(data.map(order => {
	    		return (
	    			<OrderTable key={ order._id } order = { order } />
	    		)
	    	}))
	    }).catch(error => error);
	})

	
	return (
		<>
			<h3>Order History of User ID: {user_id}</h3>
			<hr />

			<Form className="px-4 mb-4">
				<div className="table-responsive">
					<table className="table">
					<thead>
						<tr>
						<th width="170">Order ID</th>
						<th width="170">Total Amount</th>
						<th width="170">Date of Purchase</th>
						</tr>
					</thead>

					<tbody>
						{ orders }
					</tbody>
					</table>
				</div>
			</Form>	

			<div className="container d-flex align-items-center justify-content-center mb-4">
				<Link to='/admin/dashboard'><Button variant="secondary">Go Back</Button></Link>
			</div>
		</>
	)
}

