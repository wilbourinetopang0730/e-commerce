// Imported packages
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { useState, useEffect, useContext } from 'react';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Home() {

    const { user, setUser } = useContext(UserContext)

    const data = {
        title: "Welcome to Wilbo Market Place",
        content: "Home page shows the 3 current hot items of the website",
        destination: "/",
        label: "Click here"
    }
    return (
        <>  
            <Banner data={ data }/>
            <Highlights />

            <div className="container d-flex align-items-center justify-content-center mb-4">

            {
                (user.id !== null) ?
                    <p>Check out our full list of products in our <a href="/products">products</a> section.</p>
                :
                    <p>Want to see more awesome items for your fur babies? <a href="/login">Login</a> now.</p>
            }
            
            </div>
        </>
    )
}
