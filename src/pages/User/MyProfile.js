// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

// Imported Components
import OrderTable from '../../components/OrderTable';

export default function MyProfile() {
	// State hooks for the form.
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);
    // State hook to fetch the user details in API
	const [ user, setUser ] = useState([])
	const [ orders, setOrders ] = useState([])

	// Password regex validation
	const regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;

	// Fetching user's orders
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/view`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		}).then(response => response.json()).then(data => {
	    	setOrders(data.map(order => {
	    		return (
	    			<OrderTable key={ order._id } order = { order } />
	    		)
	    	}))
	    }).catch(error => error);
	})

	// Fetching user details
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/view`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		}).then(response => response.json()).then(data => setUser(data));
	}, []);

	// Textbox and Password Validation
	useEffect(() => {
    	if((password1 !== "" && password2 !== "") && (password1 === password2))
    	{
    		setIsActive(true);
    	} else {
    		setIsActive(false);
    	}
	}, [password1, password2]);


	// Function Start
    var checkPassword1 = function () {
    	if(!regularExpression.test(password1)) {
    		document.getElementById('passMessage1').innerHTML = "Minimum length of 8 and containing at least 1 upper, 1 lower, 1 numeric, and 1 special character. (a-z, A-Z, 0-9, #$%@)";
    	} else {
    		document.getElementById('passMessage1').innerHTML = "";
    	}
    }

    var checkPassword2 = function () {
    	if(password1 === password2) {
    		document.getElementById('passMessage2').innerHTML = "Passwords matched!";
    	} else {
    		document.getElementById('passMessage2').innerHTML = "Password do not match!"
    	}
    }

    function changePassword (e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

    	Swal.fire({
		  title: 'Are you sure?',
		  text: "Your password will be changed after pressing 'OK'.",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Change user password
		  	fetch(`${process.env.REACT_APP_API_URL}/users/changepassword`, {
	    		method: "PUT",
				headers: {
					"Authorization": `Bearer ${ localStorage.getItem('token') }`,
					"Content-Type": "application/json"
				}, 
				body: JSON.stringify({
	    			password: password1
	    		})
			}).then(response => response.json()).then(data => data)
		    Swal.fire({
		      title: 'Updated!',
		      text: 'Your password has been changed!',
		      icon: 'success',
		      timer: 2000
		    })

		    // Clear input fields
    		setPassword1(''); setPassword2('');
		  }
		})
    }

	return (
		(user.id !== null) ?
		<>
			<h2>Welcome back, {user.firstName}!</h2>
			<hr />
			<h3>My details</h3>

			<Form onSubmit={(e) => changePassword(e)} className="px-4 mb-4">
				{/* First Name + Last Name*/}
				<Form.Group controlId="userFullName">
					<Form.Label><b>Full Name</b></Form.Label> <br />
					<Form.Label>{user.firstName} {user.lastName}</Form.Label>
					
				</Form.Group>

				{/* Last Name */}

				{/* Email */}
				<Form.Group controlId="userEmail">
					<Form.Label><b>Email</b></Form.Label> <br />
					<Form.Label>{user.email}</Form.Label>
				</Form.Group>

				{/* Password */}
				<Form.Group controlId="password1">
					<Form.Label><b>Password</b></Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={ password1 }
						onChange={e => setPassword1(e.target.value)}
						onKeyUp = {e => checkPassword1(e.target.value)}
						required
					 />
					 <Form.Text id="passMessage1" className="registerMessage text-muted"></Form.Text>
				</Form.Group>

				{/* Confirm Password */}
				<Form.Group controlId="password2">
					<Form.Label><b>Confirm Password</b></Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={ password2 }
						onChange={e => setPassword2(e.target.value)}
						onKeyUp = {e => checkPassword2(e.target.value)}
						required
					 />
					 <Form.Text id="passMessage2" className="registerMessage text-muted">Please make sure your passwords match.</Form.Text>
				</Form.Group>

				{/*Conditional Rending for Submit Button*/}
				{
					isActive ?
						<Button variant="success" type="submit" id="submitBtn" className="mt-2" to="/login">Update Password</Button>
						:
						<Button variant="success" type="submit" id="submitBtn" className="mt-2" disabled>Update Password</Button>
				}
			</Form>

			<h3>Order History</h3>

			<Form className="px-4 mb-4">
				<div className="table-responsive">
					<table className="table">
					<thead>
						<tr>
						<th width="170">Order ID</th>
						<th width="170">Total Amount</th>
						<th width="170">Date of Purchase</th>
						</tr>
					</thead>

					<tbody>
						{ orders }
					</tbody>
					</table>
				</div>
			</Form>	

			<div className="container d-flex align-items-center justify-content-center mb-4">
				<Link to='/products'><Button variant="secondary">Go Back</Button></Link>
			</div>
		</>
		:
		<Navigate to="/login" />
	)
}

