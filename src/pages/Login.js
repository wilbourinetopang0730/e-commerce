// Import Packages
import { useState, useEffect, useContext, Link } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext)
	
	// State hooks for the text boxes.
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // State hook for the button.
    const [isActive, setIsActive] = useState(true);

    // Function Start
    function authenticate(e) {
	    // Prevents page redirection via form submission
	    e.preventDefault();

	    // Fetching the login in API
	    fetch (`${process.env.REACT_APP_API_URL}/users/auth`, {
	    	method: "POST",
	    	headers: {
	    		"Content-Type": "application/json"
	    	},
	    	body: JSON.stringify({
	    		email: email,
	    		password: password
	    	})
   	 	}).then(response => response.json()).then(data => {
   	 		if (data === false) {
   	 			Swal.fire({
	   	 			icon: 'error',
	                title: 'Authentication failed!',
	                text: 'Please check your login credentials and try again.',
	           		timer: 2000
           		})
   	 		} else {
   	 			localStorage.setItem('token', data.access);
                retireveUserDetails(data.access);
   	 			Swal.fire({
	   	 			icon: 'success',
	                title: 'Login successful!',
	                text: 'Welcome back!',
	                timer: 2000
                })
   	 		}
   	 	});
   	}
   	// Function End

   	// Fetching the viewOne in API
   	const retireveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/view/`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

	return (
        (user.id) ?
            <Navigate to="/" />
        :
            <>
            <h2>Login</h2>
            <hr />
    		<Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={ email }
            			onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={ password }
            			onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="nonUser">
                	<Form.Text>Don't have an account yet? <a href="./register">Click here</a> to register.</Form.Text>
                </Form.Group>

                <Button variant="primary" type="submit" id="submitBtn" className="mt-2">
    	                    Login
    	        </Button>
                
    		</Form>
            </>
	)

}