// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function ViewSingleProduct() {
	const { user, setUser } = useContext(UserContext)
	const [ products, setProducts ] = useState([])
	const productId = localStorage.getItem("productId");

	// State Hook for Product Quantity
	const [qty, setQty] = useState(0);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/view`, {
			method: "POST",
    		headers: {
	        		"Content-Type": "application/json"
	        },
    		body: JSON.stringify({
    			id: productId
    		})
		}).then(response => response.json()).then(data => setProducts(data));
	}, [])

	function Order (e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

    	// Pop up message using Sweet Alert 2
		Swal.fire({
		  title: 'Are you sure?',
		  text: "Order will be added after you press 'OK'",
		  icon: 'info',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	if (user.isAdmin) {
		  		Swal.fire({
			      title: 'ERROR!!',
			      text: 'User is an Admin!',
			      icon: 'warning',
			      timer: 4000
			    })
		  	}
		  	else {
		  		// Order will be added if OK was pressed in the pop up message
			  	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
					method: "POST",
		    		headers: {
		    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
		    			"Content-Type": "application/json"
		    		},
		    		body: JSON.stringify({
		    			productId: productId,
		    			quantity: qty
		    		})
				}).then(response => response.json()).then(data => data);

			  	// Pop up message will tell that order has been successfully added
				Swal.fire({
			      title: 'Added!',
			      text: 'Order has been added. You can check it in your MyProfile Order History',
			      icon: 'success',
			      timer: 4000
			    })
		  	}
		  }
		})

			
	}

	return (
		<>
			<h3>Viewing { products.name }</h3>
			<hr />

			<Form onSubmit={(e) => Order(e)}>
				{/* Product Name */}
				<Form.Group controlId="productName">
					<Form.Label><b>Product Name: </b></Form.Label><br />
					<Form.Label>{ products.name }</Form.Label>
				</Form.Group><br />

				{/* Product Description */}
				<Form.Group controlId="productDescription">
					<Form.Label><b>Product Description: </b></Form.Label><br />
					<Form.Label>{ products.description }</Form.Label>
				</Form.Group><br />

				{/* Product Price */}
				<Form.Group controlId="productPrice">
					<Form.Label><b>Product Price: </b></Form.Label><br />
					<Form.Label>{ products.price }</Form.Label>
				</Form.Group><br />

				{/* Product Quantity */}
				<Form.Group controlId="productQty">
					<Form.Label><b>Product Quantity: </b></Form.Label><br />
					<Form.Control
						type="number"
						placeholder="0"
						onChange={e => setQty(e.target.value)}
						required
					/>
				</Form.Group><br />
				{
					(user.isAdmin) ?
					<Button variant="success" className="me-2" type="submit" disabled>Buy it</Button>
					:
					<Button variant="success" className="me-2" type="submit">Buy it</Button>
				}	
				<Link to='/products'><Button variant="secondary">Go Back</Button></Link>
			</Form>
		</>
	)

}